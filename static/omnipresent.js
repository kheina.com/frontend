var updateinterval = 15 * 60000; // database update occurs every 15 minutes
var updateafter = 0.5 * 60000; // how long to wait after the database has likely update to pull updated stats (minutes)
var updatespeed = 0.5 * 1000; // how often to update the timer (seconds)
var lastUpdate = undefined;
var updateTime = undefined;
var liveupdates = true;

function commafy(x)
{ // from https://stackoverflow.com/a/2901298
	let parts = x.toString().split(".");
	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	return parts.join(".");
}

String.prototype.splice = function(idx, rem, str)
{ // from https://stackoverflow.com/a/4314050
	return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
};

function onLoad()
{	
	let data = document.getElementsByTagName('data');
	console.log('data elements:' + data.length + ' (expected:5)');

	let stats = undefined;
	let lastupdate = undefined;
	let bars = undefined;
	let subtitle = undefined;
	for (let i = 0; i < data.length; i++)
	{
		if (data[i].getAttribute('id') == 'stats')
		{ stats = data[i].innerHTML; }
		else if (data[i].getAttribute('id') == 'lastupdate')
		{ lastupdate = data[i]; }
		else if (data[i].getAttribute('id') == 'progress')
		{ bars = data[i]; }
		else if (data[i].getAttribute('id') == 'subtitle')
		{ subtitle = data[i]; }
	}

	updateStats(document.getElementById('progress'), document.getElementById('subtitle'), document.getElementById('lastupdate'));
}

function updateStats(bars, subtitle, lastupdate)
{
	fetch('https://api.kheina.com/stats?'.concat(new Date().getTime()))
	.then(response => {
		response.json()
		.then(stats =>
		{
			let nextCheck = Math.max(updateinterval - (Date.now() - stats.lastupdate * 1000), 0)
				+ (updateafter / 2 + Math.random() * updateafter); // use a random number around updateafter to avoid overloading the server unnecessarily
			setStats(stats, bars);
			setSubtitle(stats, subtitle);
			setLastUpdate(stats, lastupdate);

			setTimeout(() => updateStats(bars, subtitle, lastupdate), nextCheck);
		});
	})
	.catch(error => console.error(error));
}

function setSubtitle(stats, subtitle)
{
	if (subtitle)
	{
		let p = document.createElement('p');
		p.className = 'staticsubtitle';
		if (stats.hasOwnProperty('images'))
		{
			if (p.innerHTML.length > 0)
			{ p.innerHTML = p.innerHTML + ' - ' + commafy(stats.images) + ' images'}
			else
			{ p.innerHTML = commafy(stats.images) + ' images'}
			subtitle.style.display = 'block';
		}
		if (stats.hasOwnProperty('artists'))
		{
			if (p.innerHTML.length > 0)
			{ p.innerHTML = p.innerHTML + ' - ' + commafy(stats.artists) + ' artists'}
			else
			{ p.innerHTML = commafy(stats.artists) + ' artists'}
			subtitle.style.display = 'block';
		}
		if (stats.hasOwnProperty('sources'))
		{
			if (p.innerHTML.length > 0)
			{ p.innerHTML = p.innerHTML + ' - ' + commafy(stats.sources) + ' sources'}
			else
			{ p.innerHTML = commafy(stats.sources) + ' sources'}
			subtitle.style.display = 'block';
		}
		subtitle.innerHTML = '';
		subtitle.appendChild(p);
	}
}

function setLastUpdate(stats, lastupdate)
{
	if (lastupdate && stats.hasOwnProperty('lastupdate'))
	{
		lastUpdate = lastupdate;
		updateTime = stats.lastupdate;
		lastUpdateLive();
	}
}

function insertElapsedTime(time, ele)
{
	let updateDate = new Date(time * 1000);
	if (updateDate.getDate() != new Date().getDate())
	{ updateDate = updateDate.toLocaleString(); }
	else
	{ updateDate = updateDate.toLocaleTimeString(); }

	let relative = prettyTime((Date.now() / 1000) - time, 1) + ' ago';

	if (ele.title.endsWith('ago'))
	{
		ele.title = relative;
		ele.innerHTML = updateDate;
	}
	else
	{
		ele.innerHTML = relative;
		ele.title = updateDate;
	}

	ele.style.display = 'inline';
	ele.style.textDecorationStyle = 'dotted';
	ele.style.textDecorationLine = 'underline';
	ele.style.cursor = 'pointer';
}

function lastUpdateLive()
{
	insertElapsedTime(updateTime, lastUpdate);
	if (liveupdates)
	{ var time = setTimeout(lastUpdateLive, updatespeed); }
}

function setStats(stats, bars)
{
	if (bars)
	{
		bars.innerHTML = '';
		if (stats.hasOwnProperty('fan') && stats.hasOwnProperty('fao') && stats.fan > stats.fao)
		{
			let percent = (stats.fan - stats.fao) * 100 / stats.fan;
			let span = document.createElement('span');
			span.innerHTML = percent.toFixed(2) + '%';
			span.style.width = percent + '%';
			let p = document.createElement('p');
			p.innerHTML = 'Fur Affinity';
			if (percent > 75)
			{ p.className = 'left'; }
			else
			{ p.style.right = '4px'; }
			let a = document.createElement('a');
			a.className = 'faprogress';
			a.href = 'https://www.furaffinity.net';
			a.appendChild(span);
			a.appendChild(p);
			bars.appendChild(a);
			bars.style.display = 'block';
		}
		if (stats.hasOwnProperty('fnn') && stats.hasOwnProperty('fno') && stats.fnn > stats.fno)
		{
			let percent = (stats.fnn - stats.fno) * 100 / stats.fnn;
			let span = document.createElement('span');
			span.innerHTML = percent.toFixed(2) + '%';
			span.style.width = percent + '%';
			let p = document.createElement('p');
			p.innerHTML = 'Furry Network';
			if (percent > 75)
			{ p.className = 'left'; }
			else
			{ p.style.right = '4px'; }
			let a = document.createElement('a');
			a.className = 'fnprogress';
			a.href = 'https://furrynetwork.com';
			a.appendChild(span);
			a.appendChild(p);
			bars.appendChild(a);
			bars.style.display = 'block';
		}
	}
}

function setCookie(name, value)
{ document.cookie = name + '=' + escape(value) + '; path=/'; }

function getCookie(cname)
{
	let name = cname + '=';
	let decodedCookie = decodeURIComponent(document.cookie);
	let ca = decodedCookie.split(';');
	for (let i = 0; i < ca.length; i++)
	{
		let c = ca[i];
		while (c.charAt(0) == ' ')
		{ c = c.substring(1); }
		if (c.indexOf(name) == 0)
		{ return c.substring(name.length, c.length); }
	}
	return '';
}

function goAway()
{
	setCookie('cookies', true);
	document.getElementsByClassName('cookies')[0].style.display = 'none';
}

function checkCookies()
{
	let cookies = getCookie('cookies');
	if (cookies != 'true')
	{ document.getElementsByClassName('cookies')[0].style.display = 'block'; }	
}

function prettyTime(time, fixed=2)
{
	let conversion = 1;
	let units = 'seconds';
	if (time > 31556952)
	{
		conversion = 1 / 31556952;
		units = 'years';
	}
	else if (time > 2592000)
	{
		conversion = 1 / 2592000;
		units = 'months';
	}
	else if (time > 86400) // 24 hours in seconds
	{
		conversion = 1 / 86400;
		units = 'days';
	}
	else if (time > 5400) // 90 minutes in seconds
	{
		conversion = 1 / 3600;
		units = 'hours';
	}
	else if (time > 60)
	{
		conversion = 1 / 60;
		units = 'minutes';
	}
	else if (time < 0.000001)
	{
		conversion = 1000000000;
		units = 'nanoseconds';
	}
	else if (time < 0.001)
	{
		conversion = 1000000;
		units = 'microseconds';
	}
	else if (time < 1)
	{
		conversion = 1000;
		units = 'milliseconds';
	}
	return (time * conversion).toFixed(fixed) + ' ' + units;
}

function InsertSearchTime(stats, searchtime)
{
	try
	{ searchtime = parseFloat(searchtime); }
	catch (error)
	{
		console.error(error);
		return;
	}

	liveupdates = false;
	let p = document.createElement('p');
	p.innerHTML = 'searched ' + commafy(stats.images) + ' images from ' + commafy(stats.artists) + ' artists in ' + prettyTime(searchtime);

	return p;
}

function confirmJSON(results)
{
	for (let i = 1; i < results.length-1; i++)
	{
		if (results[i] == '"' && results[i+1] != ',' && results[i+1] != ']' && results[i-1] != '[' && results[i-1] != ',' && results[i-1] != '\\')
		{
			results = results.splice(i, 0, "\\");
			i++; // skip over the quote
		}
	}
	return results;
}

function SortData(results, iqdbdata)
{
	for (let i = 0; i < results.length; i++)
	{
		for (let j = 0; j < iqdbdata.length; j++)
		{
			if (results[i][0] == iqdbdata[j].iqdbid)
			{
				results[i][0] = iqdbdata[j];
				break;
			}
		}
		if (!results[i][0].hasOwnProperty('iqdbid'))
		{ console.error('WARNING! result iqdbid ' + results[i][0] + ' undefined in database'); }
	}
	return results;
}

function HarvestTemplates(templates)
{
	let featureResult = templates.querySelector('#templates #feature-result');
	let featureError = templates.querySelector('#templates #feature-error');
	let topresult = templates.querySelector('#templates #topresult');
	let moreresults = templates.querySelector('#templates #moreresults');
	return [featureResult, topresult, moreresults, featureError];
}
/*
iqdbdata[0] = iqdbid
iqdbdata[1] = thumbnails
iqdbdata[2] = title
iqdbdata[3] = url
iqdbdata[4] = artist
iqdbdata[5] = rating
iqdbdata[6] = websiteid
iqdbdata.similarity
*/

function FindSameImages(data)
{
	let returndata = [];
	let parent = data[0];

	for (let i = 1; i < data.length; i++)
	{
		if (((parent[2].toLowerCase() == data[i][2].toLowerCase() || parent[4].toLowerCase() == data[i][4].toLowerCase()) && CheckValueWithinMargin(parent[0].similarity, data[i][0].similarity, Math.pow(parent[0].similarity,3)/1000000))) // 1000000 = 100^3 (100 being maximum similarity)
		{
			returndata[returndata.length] = image;
		}
	}
	return returndata;
}

function SortBySize(data)
{
	for (let i = 0; i < data.length; i++)
	{
		if (data[i].length > 1) // sort by pixel count (width * height)
		{ data[i].sort(function(a, b){ return (b[7] * b[8]) - (a[7] * a[8]); }); }
	}
	return data;
}

function CheckValueWithinMargin(value1, value2, margin)
{
	if (Math.abs(value1 - value2) <= margin)
	{ return true; }
	return false;
}

function InsertTopResult(data, maxrating, matchcolor, falsecolor, template)
{
	let ele = null;
	let links = document.createElement('div');
	links.className = 'links';
	let rating = 0;
	let artists = new Set();
	if (data.sources.length > 1)
	{
		ele = document.createElement('div');

		for (let i = 0; i < data.sources.length; i++)
		{
			rating = Math.max(rating, data.sources[i].rating);
			artists.add(data.sources[i].artist);

			let a = document.createElement('a');
			a.setAttribute('href', data.sources[i].source);
			let p = document.createElement('p');
			p.innerHTML = data.sources[i].artist + ' ';
			let img = document.createElement('img');
			img.setAttribute('src', GetWebsiteImageFromID(data.sources[i].websiteid));
			a.appendChild(p);
			a.appendChild(img);
			links.appendChild(a);
		}
	}
	else 
	{
		rating = data.sources[0].rating;
		artists.add(data.sources[0].artist);
		ele = document.createElement('a');

		ele.setAttribute('href', data.sources[0].source);
		let p = document.createElement('p');
		p.innerHTML = GetWebsiteNameFromID(data.sources[0].websiteid) + ' ';
		let img = document.createElement('img');
		img.setAttribute('src', GetWebsiteImageFromID(data.sources[0].websiteid));
		links.appendChild(p);
		links.appendChild(img);
	}
	ele.className = 'source';
	ele.innerHTML = template.innerHTML;

	if (maxrating >= rating)
	{ ele.querySelector('img.thumbnail').setAttribute('src', 'https://cdn.kheina.com/file/kheinacom/' + data.sources[0].sha1 + '.jpg'); }
	else
	{
		let image = document.createElement('div');
		image.className = 'image';
		let h2 = document.createElement('h2');
		if (rating <= 1)
		{ h2.style.color = '#009DA6'; }
		else
		{ h2.style.color = '#A32121'; }
		h2.innerHTML = GetRatingFromID(rating);
		image.appendChild(h2);
		ele.querySelector('img.thumbnail').replaceWith(image);
	}

	ele.querySelector('#title').innerHTML = data.sources[0].title;

	ele.querySelector('#artist').innerHTML = (artists.size == 1 ? 'Artist: ' : 'Artists: ') + Array.from(artists).join(', ');
	ele.querySelector('#rating').innerHTML = 'Rating: ' + GetRatingFromID(rating);
	ele.querySelector('#similarity').innerHTML = 'Similarity: ' + parseFloat(data.similarity).toFixed(1) + '%';

	ele.querySelector('div.links').replaceWith(links);

	let h3 = document.createElement('h3');
	h3.id = 'percent';
	h3.style = 'color:#' + lerpColor(falsecolor, matchcolor, data.similarity / 100).toString(16).slice(1)
	h3.innerHTML = parseFloat(data.similarity).toFixed(1) + '%';
	ele.appendChild(h3);
	ele.style.height = links.clientHeight + 16; // 1em

	return ele;
}

function GetWebsiteNameFromID(ID)
{
	switch(ID)
	{
		case 0:
		return 'Fur Affinity';
		case 1:
		return 'Ink Bunny';
		case 2:
		return 'Weasyl';
		case 3:
		return 'Furry Network';
	}
}

function GetWebsiteImageFromID(ID)
{
	switch(ID)
	{
		case 0:
		return '/static/furaffinity.png';
		case 1:
		return '/static/inkbunny.png';
		case 2:
		return '/static/weasyl.png';
		case 3:
		return '/static/furrynetwork.png';
	}
}

function GetRatingFromID(ID)
{
	switch(ID)
	{
		case 0:
		return 'General';
		case 1:
		return 'Mature';
		case 2:
		return 'Explicit';
	}
}

function InsertMoreResults(iqdbdata, maxrating, matchcolor, falsecolor, template)
{
	let div = document.createElement('div');
	div.className = 'more';
	for (let i = 1; i < iqdbdata.length; i++)
	{
		let ele = null;
		let links = document.createElement('div');
		links.className = 'links';
		let artists = new Set();
		let rating = 0;

		if (iqdbdata[i].sources.length > 1)
		{
			ele = document.createElement('div');
			for (let j = 0; j < iqdbdata[i].sources.length; j++)
			{
				artists.add(iqdbdata[i].sources[j].artist);
				rating = Math.max(rating, iqdbdata[i].sources[j].rating);

				let a = document.createElement('a');
				a.setAttribute('href', iqdbdata[i].sources[j].source);
				let img = document.createElement('img');
				img.setAttribute('src', GetWebsiteImageFromID(iqdbdata[i].sources[j].websiteid));
				a.appendChild(img);
				links.appendChild(a);
			}
		}
		else
		{
			artists.add(iqdbdata[i].sources[0].artist);
			rating = iqdbdata[i].sources[0].rating;

			ele = document.createElement('a');
			ele.setAttribute('href', iqdbdata[i].sources[0].source);
			let img = document.createElement('img');
			img.setAttribute('src', GetWebsiteImageFromID(iqdbdata[i].sources[0].websiteid));
			links.appendChild(img);
		}

		ele.className = 'source';
		ele.innerHTML = template.innerHTML;

		if (maxrating >= rating)
		{ ele.querySelector('div.image img').setAttribute('src', 'https://cdn.kheina.com/file/kheinacom/' + iqdbdata[i].sources[0].sha1 + '.jpg'); }
		else
		{
			let h2 = document.createElement('h2');
			h2.className = 'centery';
			if (rating <= 1)
			{ h2.style.color = '#009DA6'; }
			else
			{ h2.style.color = '#A32121'; }
			h2.innerHTML = GetRatingFromID(rating);
			ele.querySelector('div.image img').replaceWith(h2);
		}

		ele.querySelector('div#similarity').innerHTML = '<span></span><b style="color:#' + lerpColor(falsecolor, matchcolor, iqdbdata[i].similarity / 100).toString(16).slice(1) + '">' + parseFloat(iqdbdata[i].similarity).toFixed(1) + '%</b>';

		ele.querySelector('p#artist').innerHTML = Array.from(artists).join(', ');

		ele.querySelector('div.links').replaceWith(links);
		div.appendChild(ele);
	}
	return div;
}

function lerpColor(a, b, amount)
{ // from https://gist.github.com/nikolas/b0cce2261f1382159b507dd492e1ceef
	amount = clamp(amount, 0, 1);
	const ar = a >> 16,
	ag = a >> 8 & 0xff,
	ab = a & 0xff,

	br = b >> 16,
	bg = b >> 8 & 0xff,
	bb = b & 0xff,

	rr = ar + amount * (br - ar),
	rg = ag + amount * (bg - ag),
	rb = ab + amount * (bb - ab);

	return (1 << 24) + (rr << 16) + (rg << 8) + (rb << 0);
}

function lerp(initial, target, amount)
{
	amount = clamp(amount, 0, 1);
	return initial + (target - initial) * amount;
}

function clamp(x, lowerlimit, upperlimit)
{
	if (x < lowerlimit)
	{ return lowerlimit; }
	if (x > upperlimit)
	{ return upperlimit; }
	return x;
}
