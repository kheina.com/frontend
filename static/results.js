function resultsOnLoad()
{
	let searchParams = new URLSearchParams(window.location.search.toLowerCase());
	let data = document.getElementsByTagName('data');
	console.log('data elements:' + data.length + ' (expected:8)');

	let maxrating = 0;
	if (searchParams.has('maxrating'))
	{ maxrating = searchParams.get('maxrating'); }
	else
	{
		try
		{ maxrating = getCookie('maxrating'); }
		catch
		{ /* do nothing */ }
	}
	switch(maxrating.toLowerCase())
	{
		case '1':
			maxrating = 1;
			break;
		case 'mature':
			maxrating = 1;
			break;
		case '2':
			maxrating = 2;
			break;
		case 'explicit':
			maxrating = 2;
			break;
		default:
			maxrating = 0;
			break;
	}

	let templates  = undefined;
	let results    = undefined;
	let iqdbdata   = undefined;
	let topresult  = undefined;
	let more       = undefined;
	let searchtime = undefined;
	let stats      = undefined;
	let matchcolor = 0x00ff00 // green
	let falsecolor = 0xff0000 // red
	for (let i = 0; i < data.length; i++)
	{
		switch(data[i].getAttribute('id'))
		{
			case 'templates':
				templates = HarvestTemplates(data[i]);
				break;
			case 'results':
				results = data[i].innerHTML;
				break;
			case 'iqdbdata':
				iqdbdata = data[i].innerHTML;
				break;
			case 'topresult':
				topresult = data[i];
				break;
			case 'more':
				more = data[i];
				break;
			case 'elapsedtime':
				searchtime = data[i].innerHTML;
				break;
			case 'stats':
				stats = data[i].innerHTML;
				break;
			case 'lastupdate':
				lastupdate = data[i];
				break;
			case 'progress':
				bars = data[i];
				break;
		}
	}

	results = confirmJSON(results);
	results = JSON.parse(results);
	iqdbdata = JSON.parse(iqdbdata);
	results = SortData(results, iqdbdata);

	results.sort(function(a, b){ return b[0].similarity - a[0].similarity; });
	results = FindSameImages(results);
	results = SortBySize(results);

	let topele = InsertTopResult(results[0], maxrating, matchcolor, falsecolor, templates[0]);

	topresult.parentNode.replaceChild(topele, topresult);
	topele.style.height = document.querySelector('.top .source div.links').clientHeight;
	more.parentNode.replaceChild(InsertMoreResults(results, maxrating, matchcolor, falsecolor, templates[1]), more);

	stats = JSON.parse(stats);

	let timeele = InsertSearchTime(stats, searchtime);
	document.getElementsByClassName('searchstats')[0].appendChild(timeele);
}
