function errorOnLoad()
{
	let data = document.getElementsByTagName('data');
	console.log('data elements:' + data.length + ' (expected: 3)');
	let error = '';
	let errormessage = '';
	for ( i = 0; i < data.length; i++)
	{
		if (data[i].getAttribute('id') == 'error')
		{ error = data[i]; }
		else if (data[i].getAttribute('id') == 'errormessage')
		{ errormessage = data[i]; }
	}

	let newerrormessage = document.createElement('pre');
	newerrormessage.className = 'message';
	newerrormessage.innerHTML = error.innerHTML;
	errormessage.parentNode.replaceChild(newerrormessage, errormessage);
}
