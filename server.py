from starlette.responses import FileResponse, PlainTextResponse
from pycrawl.common import GetFullyQualifiedClassName
from common.caching import SimpleCache
from HtmlTemplate import HtmlTemplate
from common import HTTPError, logging
from common.safejoin import SafeJoin
from collections import defaultdict
from urllib.parse import urlparse
from traceback import format_tb
from common.cwd import SetCWD
try : import ujson as json
except : import json
import time
import sys
import os


SetCWD()
staticDirectory = 'static'
TEMPLATE = HtmlTemplate()
logger = logging.getLogger('server')


def getTheme(cookies) :
	return f"{cookies.get('theme')} {cookies.get('accent')}".lower()


@SimpleCache(900)  # 15 minute cache
def DefaultHeaders() :
	with open('serverheaders.json') as js :
		js = json.load(js)
		for key, value in js.items() :
			js[key] = '; '.join(value)
		return js


@SimpleCache(900)  # 15 minute cache
def JSOnLoad() :
	with open('JSOnLoad.json') as js :
		js = json.load(js)
		default = js.pop('default')
		return defaultdict(lambda : default, js)


async def jsLoader(req) :
	# no cache
	return PlainTextResponse(
		'; '.join(JSOnLoad()[urlparse(req.headers.get('referer')).path]),
		headers={
			'content-type': 'application/javascript',
			'cache-control': 'no-cache',
			**DefaultHeaders(),
		}
	)


async def handle_exception(req) :
	exc_type, e, exc_tb = sys.exc_info()
	status = getattr(e, 'status', 500)

	stacktrace = format_tb(exc_tb)
	error = f'{status} {GetFullyQualifiedClassName(e)}: {e}\n	stacktrace:\n'
	for framesummary in stacktrace :
		error += framesummary
	
	if isinstance(e, HTTPError.NotFound) :
		logger.warning({
			'error': f'{status} {GetFullyQualifiedClassName(e)}: {e}',
			'stacktrace': stacktrace,
			'url': str(req.url),
			'method': req.method,
			**getattr(e, 'logdata', { }),
		})
	else :
		logger.error({
			'error': f'{status} {GetFullyQualifiedClassName(e)}: {e}',
			'stacktrace': stacktrace,
			'url': str(req.url),
			'method': req.method,
			**getattr(e, 'logdata', { }),
		})
	return error


async def HTMLErrorHandler(req, e) :
	error = await handle_exception(req)
	if issubclass(type(e), HTTPError.HTTPError) :
		status_code = e.status
		error = str(e.status) + ' ' + error
	else :
		status_code = 500
	return await TEMPLATE.format(
		SafeJoin(staticDirectory, 'error.html'),
		error=error,
		status_code=status_code,
		headers=DefaultHeaders(),
		theme=getTheme(req.cookies),
	)


# this should be the final route!
async def slash(req) :
	location = req.path_params['location']
	method = location + req.method
	try :
		safeLocation = SafeJoin(staticDirectory, location)
		if os.path.isfile(safeLocation) :			
			return FileResponse(safeLocation)
		else :
			return await TEMPLATE.format(
				safeLocation + '/index.html',
				headers=DefaultHeaders(),
				theme=getTheme(req.cookies),
			)
	except Exception as e :
		return await HTMLErrorHandler(req, e)


async def startup():
	logger.info('server started')


from starlette.applications import Starlette
from starlette.staticfiles import StaticFiles
from starlette.middleware import Middleware
from starlette.middleware.trustedhost import TrustedHostMiddleware
from starlette.routing import Route, Mount

allowed_hosts = { '127.0.0.1', 'kheina.com', '*.kheina.com' }

routes = [
	Mount('/static', app=StaticFiles(directory='static'), name='static'),
	Route('/onload.js', endpoint=jsLoader),
	Route('/{location:path}', endpoint=slash, methods=('GET', 'POST')),
]

app = Starlette(
	routes=routes,
	middleware=[Middleware(TrustedHostMiddleware, allowed_hosts=allowed_hosts)],
	on_startup=[startup],
)

if __name__ == '__main__' :
	from uvicorn.main import run
	run(app, host='127.0.0.1', port=80)
